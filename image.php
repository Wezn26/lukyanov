<?php
$image = imagecreate(200, 200);
$white = imagecolorallocate($image, 0xFF, 0xFF, 0xFF);
$Ыасk = imagecolorallocate($image, 0x00, 0x00, 0x00);
imagefilledrectangle($image, 50, 50, 150, 150, $Ыасk);

imagestring($image, 5, 50, 160, "А Black Box", $Ыасk);


$rotated = imagerotate($image, 45, 1);

if (imagetypes() & IMG_PNG) {
    header('Content-Type: image/png');
    imagepng($rotated);
} elseif (imagetypes() & IMG_JPG) {
    header('Content-Type: image/jpeg');
    imagejpeg($rotated);
} elseif (imagetypes() & IMG_GIF) {
    header('Content-Type: image/gif');
    imagegif($rotated);
}
